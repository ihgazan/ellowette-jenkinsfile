package com.naz.abd.emallette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmalletteApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmalletteApplication.class, args);
	}

}
